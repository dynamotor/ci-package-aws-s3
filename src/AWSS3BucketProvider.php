<?php 

namespace Dynamotor\Extended\FileProvider\AWSS3;



use \Aws\S3\S3Client;

use \Dynamotor\Modules\FileModule;
use \Dynamotor\Modules\IFileBucketProvider;
use \Dynamotor\Core\HC_Module;
use \Dynamotor\Core\HC_Exception;

/**
 * An adapter classe for using AWS S3 as one of file provider in system.
 * 
 * AWS PHP SDK is required. 
 * 
 * @category Class
 * @package Dynamotor\Extended
 */
class AWSS3BucketProvider extends HC_Module implements IFileBucketProvider
{

	protected $bucket_id;
	protected $bucket_region;
	protected $access_key;
	protected $secret_key;
	protected $public_url;

    /**
     * The constructor
     *
     * @param array $config
     */
	public function __construct($config = [])
	{

		if(isset($config['bucket_id'])){
			$this->bucket_id = $config['bucket_id'];
		}
		if(isset($config['bucket_region'])){
			$this->bucket_region = $config['bucket_region'];
		}
		if(isset($config['access_key'])){
			$this->access_key = $config['access_key'];
		}
		if(isset($config['secret_key'])){
			$this->secret_key = $config['secret_key'];
		}
		if(isset($config['public_url'])){
			$this->public_url = $config['public_url'];
		}
	}

	/**
	 * Returns the MIME types array from config/mimes.php
	 *
	 * @return	array
	 */
	public function &get_mimes()
	{
		static $mimes;

		if (empty($mimes))
		{
			if (file_exists(APPPATH.'config/'.ENVIRONMENT.'/mimes.php'))
			{
				include(APPPATH.'config/'.ENVIRONMENT.'/mimes.php');
			}
			elseif (file_exists(APPPATH.'config/mimes.php'))
			{
				include(APPPATH.'config/mimes.php');
			}
			else
			{
				$mimes = array();
			}
		}

		return $mimes;
	}

	public function get_client()
	{
		if(!$this->client){

			$config = [
				'region'=>$this->bucket_region,
				'version'  => '2006-03-01',
				'credentials' => [
					'key'=>$this->access_key,
					'secret'=>$this->secret_key,
				]
			];

			//log_message('debug',__METHOD__.'.config='.print_r($config, true));

			$this->client = S3Client::factory($config);
		}
		return $this->client;
	}
	

	public function get_bucket_source()
	{
		return 'AWS-S3';
	}

	protected $file_module;

	public function set_parent(FileModule $file_module)
	{
		$this->file_module = $file_module;
	}


	public function put_data(&$file_record, $file_data)
	{
		$bucket_path = $this->file_module->get_path(data('folder',$file_record),$file_record['sys_name']);
	    $object_key = $this->file_module->get_private_storage_path($bucket_path);

	    $cfg = [
	        'Bucket'      => $this->bucket_id,
	        'Key' =>  $object_key ,
	        'ACL'         => 'private',
	    ];

	    if(!empty($file_record['file_name']))
	    	$cfg['ContentType'] = get_mime_by_extension($file_record['file_name']);

	    if(empty($cfg['ContentType']))
	    	unset($cfg['ContentType']);

	    // log_message('debug',__METHOD__.'@'.__LINE__.', put_data, cfg='.print_r($cfg, true));

	    $cfg['Body'] = $file_data;

	    $s3_object = $this->get_client()->putObject($cfg)->toArray();

	    if(empty($s3_object)){
	    	return $this->error(FileModule::STATUS_ERROR_UPLOAD_OBJECT, 'FileServiceAWSS3UploadObjectError');
	    }


    	$file_record['bucket_source'] = $this->get_bucket_source();
    	$file_record['bucket_id'] = $this->bucket_id;
    	$file_record['bucket_path'] = $bucket_path;
	}

	public function put_file(&$file_record, $file_path)
	{
		$bucket_path = $this->file_module->get_path(data('folder',$file_record),$file_record['sys_name']);
	    $object_key = $this->file_module->get_private_storage_path($bucket_path);

	    $cfg = [
	        'Bucket'      => $this->bucket_id,
	        'Key' =>  $object_key,
	        'ACL'         => 'private',
	        'SourceFile'  => $file_path,
	    ];

	    if(!empty($file_record['file_name']))
	    	$cfg['ContentType'] = get_mime_by_extension($file_record['file_name']);

	    if(empty($cfg['ContentType']))
	    	unset($cfg['ContentType']);

	    log_message('debug',__METHOD__.'@'.__LINE__.', put_file, cfg='.print_r($cfg, true));

	    $s3_object = $this->get_client()->putObject($cfg)->toArray();

	    if(empty($s3_object)){
	    	return $this->error(FileModule::STATUS_ERROR_UPLOAD_OBJECT, 'FileServiceAWSS3UploadObjectError');
	    }


    	$file_record['bucket_source'] = $this->get_bucket_source();
    	$file_record['bucket_id'] = $this->bucket_id;
    	$file_record['bucket_path'] = $bucket_path;
	}

	public function get_data($file_record)
	{
		if(empty($file_record['bucket_path'])){
			log_message('error',__METHOD__.'#FileDataBucketPathRequired, file='.print_r($file_resource, true));
			return $this->error(ERROR_INVALID_DATA, 'FileDataBucketPathRequired');
		}

		$bucket_path = $file_record['bucket_path'];
	    $object_key = $this->file_module->get_private_storage_path($bucket_path);

		$cfg = [
	        'Bucket'      => $this->bucket_id,
	        'Key' =>$object_key,
	    ];
		$s3_object = $this->get_client()->getObject($cfg)->toArray();

		if(empty($s3_object['Body']))
			return $this->error(ERROR_INVALID_DATA, 'FileServiceDataNotFound');


		return $s3_object['Body'];
	}

	public function remove_data($file_record)
	{
		if(empty($file_record['bucket_path'])){
			log_message('error',__METHOD__.'#FileDataBucketPathRequired, file='.print_r($file_resource, true));
			return $this->error(ERROR_INVALID_DATA, 'FileDataBucketPathRequired');
		}
		$bucket_path = $file_record['bucket_path'];
	    $object_key = $this->file_module->get_private_storage_path($bucket_path);

		$s3_object = $this->get_client()->deleteObject([
	        'Bucket'      => $this->bucket_id,
	        'Key' => $object_key,
	    ])->toArray();

	    if(empty($aws_object)){
	    	return $this->error(self::STATUS_ERROR_UPLOAD_OBJECT, 'FileServiceRemoveObjectError');
	    }
	}


	public function is_resource_exist($file_record, $file_resource)
	{
		if(empty($file_resource['bucket_path'])){
			log_message('error',__METHOD__.'#FileResourceBucketPathRequired, resource='.print_r($file_resource, true));
			return $this->error(ERROR_INVALID_DATA, 'FileResourceBucketPathRequired');
		}
		$bucket_path = $file_resource['bucket_path'];
	    $object_key = $this->file_module->get_public_storage_path($bucket_path);

		return $this->get_client()->doesObjectExist($this->bucket_id,$object_key );
	}

	public function get_resource_url($file_record, $file_resource)
	{
		if(empty($file_resource['bucket_path'])){
			log_message('error',__METHOD__.'#FileResourceBucketPathRequired, resource='.print_r($file_resource, true));
			return $this->error(ERROR_INVALID_DATA, 'FileResourceBucketPathRequired');
		}
		$bucket_id  = $this->bucket_id;
		if(!empty($file_resource['bucket_id'])){
			$bucket_id = $file_resource['bucket_id'];
		}

		$bucket_path = $file_resource['bucket_path'];
	    $object_key = $this->file_module->get_public_storage_path($bucket_path);

		$plainUrl = $this->get_client()->getObjectUrl($bucket_id, $object_key);

		if(empty($plainUrl)){
			log_message('error',__METHOD__.'@'.__LINE__.', FileResourceNotFound, data='.print_r(compact('plainUrl','file_resource'), true));
			return $this->error(ERROR_INVALID_DATA, 'FileResourceNotFound');
		}

		if(!empty($this->public_url)){
			$plainUrl = $this->file_module->get_path($this->public_url, $object_key);;
			log_message('debug',__METHOD__.'@'.__LINE__.', FileResourceUrlReplacedWithPublicUrl, plainUrl='.$plainUrl);

			return $plainUrl;
		}

		return $plainUrl;
	}

	public function put_resource_data($file_record, &$file_resource, $new_data = null)
	{
		if(empty($file_resource['bucket_path'])){
			log_message('error',__METHOD__.'#FileResourceBucketPathRequired, resource='.print_r($file_resource, true));
			return $this->error(ERROR_INVALID_DATA, 'FileResourceBucketPathRequired');
		}
		$bucket_path = $file_resource['bucket_path'];
	    $object_key = $this->file_module->get_public_storage_path($bucket_path);

	    $cfg = [
	        'Bucket'      => $this->bucket_id,
	        'Key' => $object_key,
	        'ACL'         => 'public-read',
	    ];

	    // Set content-type to the object
	    if(!empty($file_resource['parameters']['mime_type']))
	    	$cfg['ContentType'] = $file_resource['parameters']['mime_type'];

	    if(empty($cfg['ContentType']) && !empty($file_record['file_name']))
	    	$cfg['ContentType'] = get_mime_by_extension($file_record['file_name']);;

	    if(empty($cfg['ContentType']))
	    	unset($cfg['ContentType']);

	    $cfg['Body'] = $new_data;

	    log_message('debug',__METHOD__.'@'.__LINE__.', put_resource_data, cfg='.print_r(compact('file_record','cfg'), true));

		$s3_object = $this->get_client()->putObject($cfg)->toArray();

	    if(empty($s3_object['ObjectURL'])){
			log_message('error',__METHOD__.'@'.__LINE__.', FileResourceCreateError, data='.print_r(compact('s3_object','file_resource'), true));
	    	return $this->error(self::STATUS_ERROR_UPLOAD_OBJECT, 'FileResourceCreateError');
	    }

		log_message('debug',__METHOD__.'@'.__LINE__.', FileResourceCreated, data='.print_r(compact('s3_object','file_resource'), true));

	    $file_resource['bucket_source'] = $this->get_bucket_source();
	    $file_resource['bucket_id'] = $this->bucket_id;
	    //$file_resource['bucket_path'] = $bucket_path;


		if(!empty($this->public_url)){
			$plainUrl = $this->file_module->get_path($this->public_url, $object_key);;
			log_message('debug',__METHOD__.'@'.__LINE__.', FileResourceUrlReplacedWithPublicUrl, plainUrl='.$plainUrl);

			return $plainUrl;
		}
		
	    return $s3_object['ObjectURL'];
	}

	public function put_resource_file($file_record, &$file_resource, $new_file_path = null)
	{
		if(empty($file_resource['bucket_path'])){
			log_message('error',__METHOD__.'#FileResourceBucketPathRequired, resource='.print_r($file_resource, true));
			return $this->error(ERROR_INVALID_DATA, 'FileResourceBucketPathRequired');
		}
		$bucket_path = $file_resource['bucket_path'];
	    $object_key = $this->file_module->get_public_storage_path($bucket_path);

	    $cfg = [
	        'Bucket'      => $this->bucket_id,
	        'Key' => $object_key,
	        'ACL'         => 'public-read',
	        'SourceFile'=>$new_file_path,
	    ];

	    // Set content-type to the object
	    if(!empty($file_resource['parameters']['mime_type']))
	    	$cfg['ContentType'] = $file_resource['parameters']['mime_type'];

	    if(empty($cfg['ContentType']) && !empty($file_record['file_name']))
	    	$cfg['ContentType'] = get_mime_by_extension($file_record['file_name']);;

	    if(empty($cfg['ContentType']))
	    	unset($cfg['ContentType']);

	    log_message('debug',__METHOD__.'@'.__LINE__.', put_resource_data, cfg='.print_r($cfg, true));

	    if(empty($s3_object['ObjectURL'])){
			log_message('error',__METHOD__.'@'.__LINE__.', FileResourceCreateError, data='.print_r(compact('s3_object','file_resource'), true));
	    	return $this->error(self::STATUS_ERROR_UPLOAD_OBJECT, 'FileResourceCreateError');
	    }

		log_message('debug',__METHOD__.'@'.__LINE__.', FileResourceCreated, data='.print_r(compact('s3_object','file_resource'), true));

	    $file_resource['bucket_source'] = $this->get_bucket_source();
	    $file_resource['bucket_id'] = $this->bucket_id;
	    //$file_resource['bucket_path'] = $bucket_path;


		if(!empty($this->public_url)){
			$plainUrl = $this->file_module->get_path($this->public_url, $object_key);;
			log_message('debug',__METHOD__.'@'.__LINE__.', FileResourceUrlReplacedWithPublicUrl, plainUrl='.$plainUrl);

			return $plainUrl;
		}

	    return $s3_object['ObjectURL'];
	}

	public function remove_resource($file_record, $file_resource)
	{
		$bucket_path = $file_resource['bucket_path'];
	    $object_key = $this->file_module->get_public_storage_path($bucket_path);

		$s3_object = $this->get_client()->deleteObject([
	        'Bucket'      =>$file_resource['bucket_id'],
	        'Key' => $object_key,
	    ])->toArray();

	    if(empty($s3_object)){
	    	return $this->error(self::STATUS_ERROR_UPLOAD_OBJECT, 'FileResourceRemoveObjectError');
	    }
	}
}